from django.contrib import admin
from .models import Employee, Projects, Skills

class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('first_name',
                    'second_name',
                    'third_name',
                    'profession',
                    'experience',
                    'cellphone',
                    'projects',
                    'skills',
                    'email',
                    )

class ProjectsAdmin(admin.ModelAdmin):
    list_display = ('name',
                    'begin',
                    'end',
                    'budget'
                    )

class SkillsAdmin(admin.ModelAdmin):
    list_display = ('adept',
                    'actor',
                    'analyst',
                    'fighter',
                    'visionary',
                    'leader',
                    'intuit',
                    'filler',
                    'constructor',
                    'controller',
                    'organizer',
                    'chaser',
                    'connected',
                    'creator',
                    'hacker',
                    'master',
                    'experimenter',
                    'empath',
                    'esthete',
                    'expert'
                    )

admin.site.register(Employee)
admin.site.register(Projects)
admin.site.register(Skills)
