from django.conf import settings
from django.db import models
from datetime import date

class Employee(models.Model):
    first_name = models.CharField("person's first name", max_length=30, null=True) # Имя
    second_name = models.CharField("person's second name", max_length=30, null=True) # Фамилия
    third_name = models.CharField("person's third name", max_length=30, null=True) # Отчество
    profession = models.CharField(max_length=200, null=True) # Профессия
    experience = models.CharField("Years of work", max_length=200, null=True) # Опыт работы
    cellphone = models.CharField(max_length=30, null=True) # Моб. телефон
    projects = models.ForeignKey('Projects', on_delete=models.CASCADE,)# Номер проекта
    skills = models.ForeignKey('Skills', on_delete=models.CASCADE)
    email = models.CharField(max_length=40, null=True)        # Електронная почта

class Projects(models.Model):
    name = models.CharField(max_length=200, null=True, unique=True) # Название проекта
    begin = models.DateField(auto_now=False, auto_now_add=False, default=date.today) # Дата начала проекта
    end = models.DateField(auto_now=False, auto_now_add=False, default=date.today) # Дата окончания проекта
    budget = models.FloatField(max_length=200, null=True, default=10000.0) # Бюджет проекта

class Skills(models.Model):
    adept = models.CharField(max_length=2, null=True,  default=0,)        # Адепт
    actor = models.CharField(max_length=2, null=True,  default=0,)         # Актёр
    analyst = models.CharField(max_length=2, null=True,  default=0,)       # Аналитик
    fighter = models.CharField(max_length=2, null=True,  default=0,)       # Боец
    visionary = models.CharField(max_length=2, null=True,  default=0,)     # Визионер
    leader = models.CharField(max_length=2, null=True,  default=0,)        # Вождь
    intuit = models.CharField(max_length=2, null=True,  default=0,)        # Интуит
    filler = models.CharField(max_length=2, null=True,  default=0,)        # Исполнитель
    constructor = models.CharField(max_length=2, null=True,  default=0,)   # Конструктор
    controller = models.CharField(max_length=2, null=True,  default=0,)    # Контролёр
    organizer = models.CharField(max_length=2, null=True,  default=0,)     # Организатор
    chaser = models.CharField(max_length=2, null=True,  default=0,)        # Преследователь
    connected = models.CharField(max_length=2, null=True,  default=0,)     # Связной
    creator = models.CharField(max_length=2, null=True,  default=0,)       # Творец
    hacker = models.CharField(max_length=2, null=True,  default=0,)        # Хакер
    master = models.CharField(max_length=2, null=True,  default=0,)        # Хозяин
    experimenter = models.CharField(max_length=2, null=True,  default=0,)  # Экспериментатор
    empath = models.CharField(max_length=2, null=True,  default=0,)        # Эмпат
    esthete = models.CharField(max_length=2, null=True,  default=0,)       # Эстет
    expert = models.CharField(max_length=2, null=True,  default=0,)        # Эксперт
