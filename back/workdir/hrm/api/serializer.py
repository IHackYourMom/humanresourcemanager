from rest_framework import serializers
from . models import Employee
from . models import Projects
from . models import Skills

class EmployeeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Employee
        fields = '__all__'

class ProjectsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Projects
        fields = '__all__'

class SkillsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skills
        fields = '__all__'
