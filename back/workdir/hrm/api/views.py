from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Employee
from .models import Projects
from .models import Skills
from .serializer import EmployeeSerializer
from .serializer import ProjectsSerializer
from .serializer import SkillsSerializer
from rest_framework.generics import UpdateAPIView
from rest_framework import generics
from rest_framework.permissions import IsAdminUser

zaproz=["suka"]


class SkillsList(APIView):

    def get(self, request):
        skills1 = Skills.objects.all()
        serializer = SkillsSerializer(skills1, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        zapros = request.data["profession"]
        objects = Employee.objects.filter(profession=zapros)
        serializer = EmployeeSerializer(objects, many=True)
        return Response(serializer.data)

class EmployeeList(APIView):

    def get(self, request):
        Employee1 = Employee.objects.all()
        serializer = EmployeeSerializer(Employee1, many=True)
        return Response(serializer.data)


    def post(self, request, format=None):
        print(zaproz)
        zapros = request.data["profession"]
        objects = Employee.objects.filter(profession=zapros)
        serializer = EmployeeSerializer(objects, many=True)
        return Response(serializer.data)

class ProjectsList(APIView):

    def get(self, request):
        Projects1 = Projects.objects.all()
        serializer = ProjectsSerializer(Projects1, many=True)
        return Response(serializer.data)

    def post(self, request):
        print(request.data)
        newProject = ProjectsSerializer(data=request.data)

        if newProject.is_valid():
            newProject.save()
        else:
            print(newProject.errors)

        return Response(newProject.data)

class ProjectsEdit(UpdateAPIView):
    queryset = Projects.objects.all()
    serializer_class = ProjectsSerializer
    permission_classes = (IsAdminUser,)

    def  list (self, request):
        queryset = self.get_queryset()
        serializer = ProjectsSerializer(queryset, many=True)
        return Response(serializer.data)

class EmployeeEdit(UpdateAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = (IsAdminUser,)

    def list(self, request):
        queryset = self.get_queryset()
        serializer = EmployeeSerializer(queryset, many=True)
        return Response(serializer.data)

class SkillsEdit(UpdateAPIView):
    queryset = Skills.objects.all()
    serializer_class = SkillsSerializer
    permission_classes = (IsAdminUser,)

    def list(self, request):
        queryset = self.get_queryset()
        serializer = SkillsSerializer(queryset, many=True)
        return Response(serializer.data)

class ProjectOne(APIView):

    def get(self, request, pk):
        Projects1 = Projects.objects.get(pk=pk)
        serializer = ProjectsSerializer(Projects1, many=False)
        return Response(serializer.data)


    def post(self):
        pass

class EmployeeOne(APIView):

    def get(self, request, pk):
        Employee1 = Employee.objects.get(pk=pk)
        serializer = EmployeeSerializer(Employee1, many=False)
        return Response(serializer.data)


    def post(self):
        pass

class SkillsOne(APIView):

    def get(self, request, pk):
        Skills1 = Skills.objects.get(pk=pk)
        serializer = SkillsSerializer(Skills1, many=False)
        return Response(serializer.data)


    def post(self):
        pass
