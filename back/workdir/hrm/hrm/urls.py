
from django.contrib import admin
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from api import views

urlpatterns = [
        path('admin/', admin.site.urls),
        path('employee/', views.EmployeeList.as_view()),
        path('projects/', views.ProjectsList.as_view()),
        path('projects/<int:pk>', views.ProjectOne.as_view()),
        path('employee/<int:pk>', views.EmployeeOne.as_view()),
        path('skills/<int:pk>', views.SkillsOne.as_view()),
        path('skills/', views.SkillsList.as_view()),
        path('projects/<int:pk>/edit', views.ProjectsEdit.as_view()),
        path('employee/<int:pk>/edit', views.EmployeeEdit.as_view()),
        path('skills/<int:pk>/edit', views.SkillsEdit.as_view()),
]
